import os


class HelloWorld:

    def __init__(self):
        self.msg = "Hello World!"
    
    def __call__(self):
        print(self.msg)
